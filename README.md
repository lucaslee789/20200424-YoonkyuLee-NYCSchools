This is the Code Challenge from JPMorgan Chase. 

It is an Android App that provides information on NYC Schools. 

I got the data from below links

Display a list of NYC High Schools:
https://data.cityofnewyork.us/Education/DOE-High-School-Directory-2017/s3k6-pzi2

Display all the SAT Scores:
https://data.cityofnewyork.us/Education/SAT-Results/f9bf-2cp4

This project follows the MVVM (Model - View - ViewModel) Pattern.
It uses Retrofit to fetch JSON from URL. 

Instructions:

1. On the first page, enter the code to see the school list. 
    - If you enter the right code, you will be able to direct to the next page
    - I built the Instrumented Unit Test for this case. 
2. On the second page, you will see all the list of schools with name, borough, and phone number.
    - It is sorted by alphabetical ascending order.
    - You can click the schools to see the details.
3. On the third page, you will see all the details of the school with the name, address, website(clickable), 
   SAT scores, number of students who took the SAT, and school overview. 
   - If you want to share the school information, you can click the share button. 