package com.example.a20200424_yoonkyulee_nycschools.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;

import com.example.a20200424_yoonkyulee_nycschools.R;
import com.example.a20200424_yoonkyulee_nycschools.adapter.SchoolAdapter;
import com.example.a20200424_yoonkyulee_nycschools.model.SchoolLlist;
import com.example.a20200424_yoonkyulee_nycschools.viewmodel.SchoolViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SchoolListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    SchoolAdapter adapter;

    private final ArrayList<SchoolLlist> schoolList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle(R.string.list_action);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_list);
        //Call the RecyclerView
        recyclerView = findViewById(R.id.SchoolRecyclerView);
        SchoolRecyclerView();

        //Get the instance of SchoolViewModel
        //Got the concept information below
        //https://proandroiddev.com/mvvm-with-kotlin-android-architecture-components-dagger-2-retrofit-and-rxandroid-1a4ebb38c699
        final SchoolViewModel schoolViewModel = new ViewModelProvider(this).get(SchoolViewModel.class);
        schoolViewModel.getSchools().observe(this, new Observer<ArrayList<SchoolLlist>>() {
            @Override
            public void onChanged(@Nullable ArrayList<SchoolLlist> schools) {
                schoolList.addAll(schools);
                //Sort by alphabetical ascending order
                Collections.sort(schoolList, new Comparator<SchoolLlist>() {
                    @Override
                    public int compare(SchoolLlist o1, SchoolLlist o2) {
                        return o1.getSchoolName().compareTo(o2.getSchoolName());
                    }
                });
                adapter.notifyDataSetChanged();
            }
        });
    }
    //Method to call the RecyclerView
    private void SchoolRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager
                (this, LinearLayoutManager.VERTICAL, false);
        if (adapter == null) {
            adapter = new SchoolAdapter(this, schoolList);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setNestedScrollingEnabled(true);
        } else {
            adapter.notifyDataSetChanged();
        }
    }
}
