package com.example.a20200424_yoonkyulee_nycschools.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import android.content.Intent;
import android.os.Bundle;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.a20200424_yoonkyulee_nycschools.R;
import com.example.a20200424_yoonkyulee_nycschools.model.SchoolDetail;
import com.example.a20200424_yoonkyulee_nycschools.viewmodel.SchoolDetailViewModel;

import java.util.ArrayList;

public class SchoolDetailActivity extends AppCompatActivity {

    //SchoolDetail info
    TextView SchoolName;
    TextView SchoolLocation;
    TextView SchoolWebsite;
    TextView SatScores;
    TextView NumStudents;
    TextView ReadingAvg;
    TextView MathAvg;
    TextView WritingAvg;
    TextView SchoolOverview;
    TextView SchoolView;

    //SchoolList info
    private String name;
    private String location;
    private String website;
    private String overview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle(R.string.detail_action);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_detail);
        //Get the data that was sent by intent from Adapter.
        Intent intent = getIntent();
        String dbn = intent.getStringExtra("DBN");
        name = intent.getStringExtra("NAME");
        location = intent.getStringExtra("LOCATION");
        website = intent.getStringExtra("WEBSITE");
        overview = intent.getStringExtra("OVERVIEW");
        Log.v("myLogs", "DBN " + dbn);

        //Define the TextView
        SchoolName = findViewById(R.id.SchoolName);
        SchoolLocation = findViewById(R.id.Location);
        SchoolWebsite = findViewById(R.id.SchoolWebsite);
        SatScores = findViewById(R.id.SatScores);
        NumStudents = findViewById(R.id.Students);
        ReadingAvg = findViewById(R.id.CriticalReadingAvg);
        MathAvg = findViewById(R.id.MathAvg);
        WritingAvg = findViewById(R.id.WritingAvg);
        SchoolOverview = findViewById(R.id.SchoolOverview);
        SchoolView = findViewById(R.id.Overview);

        //Get the instance of SchoolDetailViewModel
        //Got the concept information below
        //https://proandroiddev.com/mvvm-with-kotlin-android-architecture-components-dagger-2-retrofit-and-rxandroid-1a4ebb38c699
        final SchoolDetailViewModel schoolDetailViewModel = new ViewModelProvider(this).get(SchoolDetailViewModel.class);
        schoolDetailViewModel.getSchoolDetails(dbn).observe(this, new Observer<ArrayList<SchoolDetail>>() {
            @Override
            public void onChanged(@Nullable ArrayList<SchoolDetail> schoolDetails) {
                //If there are no SAT Scores, assign the below info.
                if (schoolDetails.isEmpty()){
                    SchoolName.setText(name);
                    location = location.split("\\(")[0];
                    SchoolLocation.setText(location);
                    SchoolWebsite.setText(website);
                    Linkify.addLinks(SchoolWebsite, Linkify.ALL);
                    SatScores.setText(R.string.no_sat);
                    NumStudents.setText("");
                    ReadingAvg.setVisibility(View.GONE);
                    MathAvg.setVisibility(View.GONE);
                    WritingAvg.setVisibility(View.GONE);
                    SchoolOverview.setText(R.string.school_overview);
                    SchoolView.setText(overview);
                } else {
                    //If there are SAT Scores, assign the below info.
                    String schoolName = schoolDetails.get(0).getSchoolName();
                    String numStudents = schoolDetails.get(0).getNumOfSatTestTakers();
                    String criticalReadingAvgScore = schoolDetails.get(0).getSatCriticalReadingAvgScore();
                    String satMathAvgScore = schoolDetails.get(0).getSatMathAvgScore();
                    String satWritingAvgScore = schoolDetails.get(0).getSatWritingAvgScore();
                    SchoolName.setText(schoolName);
                    location = location.split("\\(")[0];
                    SchoolLocation.setText(location);
                    SchoolWebsite.setText(website);
                    Linkify.addLinks(SchoolWebsite, Linkify.ALL);
                    SatScores.setText(R.string.sat_scores);
                    NumStudents.setText(getResources().getString(R.string.num_students) + (numStudents));
                    ReadingAvg.setText(getResources().getString(R.string.reading_avg) + (criticalReadingAvgScore));
                    MathAvg.setText(getResources().getString(R.string.math_avg) + (satMathAvgScore));
                    WritingAvg.setText(getResources().getString(R.string.writing_avg) + (satWritingAvgScore));
                    SchoolOverview.setText(R.string.school_overview);
                    SchoolView.setText(overview);
                }
            }
        });
    }

    public void Clicked(View view)
    {  
       //Share the Name, Address, Website, and Overview when the button is clicked
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,"Name: "+ name + "\n" + "Address: " + location + "\n" + "Website: " + website + "\n" + "\n" + "Overview:" + "\n" + overview);
        sendIntent.setType("text/plain");
        Intent.createChooser(sendIntent,"Share via");
        startActivity(sendIntent);
    }
}
