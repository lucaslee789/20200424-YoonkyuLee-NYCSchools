package com.example.a20200424_yoonkyulee_nycschools.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//Got the concept information below
//https://www.simplifiedcoding.net/retrofit-android-example/
public class SchoolLlist {

    @SerializedName("dbn")
    @Expose
    private String dbn;

    @SerializedName("school_name")
    @Expose
    private String schoolName;

    @SerializedName("website")
    @Expose
    private String website;

    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;

    @SerializedName("borough")
    @Expose
    private String borough;

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("overview_paragraph")
    @Expose
    private String overview;

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsiteW(String website) {
        this.website = website;
    }

    public String getPhoneNumber() {return phoneNumber;}

    public void setPhoneNumber(String phoneNumber) {this.phoneNumber = phoneNumber;}

    public String getBorough() { return borough; }

    public void setBorough(String borough) { this.borough = borough; }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }



}
