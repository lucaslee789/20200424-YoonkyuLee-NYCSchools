package com.example.a20200424_yoonkyulee_nycschools.remoteAPI;

import com.example.a20200424_yoonkyulee_nycschools.model.SchoolDetail;
import com.example.a20200424_yoonkyulee_nycschools.model.SchoolLlist;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

//Retrofit - API Interface
//Got the concept information below
//https://www.simplifiedcoding.net/retrofit-android-example/
public interface SchoolAPI {
    String BASE_URL = "https://data.cityofnewyork.us/resource/";

    //Define an http GET request. It makes the complete URL joining the BASE_URL and the api name
    //The type of the Call is an ArrayList
    @GET("97mf-9njv.json?$select=dbn,school_name,borough,phone_number,website,location,overview_paragraph")
    Call<ArrayList<SchoolLlist>> getSchools();

    @GET("734v-jeq5.json?")
    Call<ArrayList<SchoolDetail>> getSchoolDetails(@Query("dbn") String dbn);
}
