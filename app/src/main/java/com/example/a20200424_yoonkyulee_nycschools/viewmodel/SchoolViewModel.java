package com.example.a20200424_yoonkyulee_nycschools.viewmodel;

import com.example.a20200424_yoonkyulee_nycschools.model.SchoolLlist;
import com.example.a20200424_yoonkyulee_nycschools.remoteAPI.SchoolAPI;

import java.util.ArrayList;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SchoolViewModel extends ViewModel {
    //Got the concept information below
    //https://stackoverflow.com/questions/55914752/when-to-use-mutablelivedata-and-livedata/55915079
    private MutableLiveData<ArrayList<SchoolLlist>> schoolList;

    //Get the School data
    public LiveData<ArrayList<SchoolLlist>> getSchools() {
        //Asynchronously load data
        if (schoolList == null) {
            schoolList = new MutableLiveData<>();
            loadSchools();
        }
        return schoolList;
    }

    //Retrofit - Fetch JSON from URL
    //Make the API Call
    //Got the concept information below
    //https://www.simplifiedcoding.net/retrofit-android-example/
    private void loadSchools() {
        //Create a Retrofit object
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SchoolAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //Create the API interface
        SchoolAPI api = retrofit.create(SchoolAPI.class);
        //Call the object
        Call<ArrayList<SchoolLlist>> call = api.getSchools();
        //Make the call using enqueue which takes callback interface as an argument
        call.enqueue(new Callback<ArrayList<SchoolLlist>>() {
            //If the request is successful
            @Override
            public void onResponse(Call<ArrayList<SchoolLlist>> call, Response<ArrayList<SchoolLlist>> response) {
                //Get the School list
                schoolList.setValue(response.body());
            }
            //If the request is not successful
            @Override
            public void onFailure(Call<ArrayList<SchoolLlist>> call, Throwable t) {
                schoolList.setValue(null);
            }
        });
    }
}
