package com.example.a20200424_yoonkyulee_nycschools.viewmodel;

import com.example.a20200424_yoonkyulee_nycschools.model.SchoolDetail;
import com.example.a20200424_yoonkyulee_nycschools.remoteAPI.SchoolAPI;

import java.util.ArrayList;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SchoolDetailViewModel extends ViewModel {
    //Got the concept information below
    //https://stackoverflow.com/questions/55914752/when-to-use-mutablelivedata-and-livedata/55915079
    private MutableLiveData<ArrayList<SchoolDetail>> schoolDetailsList;

    //Get the SchoolDetail data
    public LiveData<ArrayList<SchoolDetail>> getSchoolDetails(String dbn) {
        //Asynchronously load data
        if (schoolDetailsList == null) {
            schoolDetailsList = new MutableLiveData<>();
            loadSchoolDetails(dbn);
        }
        return schoolDetailsList;
    }

    //Retrofit - Fetch JSON from URL
    //Make the API Call
    //Got the concept information below
    //https://www.simplifiedcoding.net/retrofit-android-example/
    private void loadSchoolDetails(String dbn) {
        //Create a Retrofit object
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SchoolAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //Create the API interface
        SchoolAPI api = retrofit.create(SchoolAPI.class);
        //Call the object
        Call<ArrayList<SchoolDetail>> call = api.getSchoolDetails(dbn);
        //Make the call using enqueue which takes callback interface as an argument
        call.enqueue(new Callback<ArrayList<SchoolDetail>>() {
            //If the request is successful
            @Override
            public void onResponse(Call<ArrayList<SchoolDetail>> call, Response<ArrayList<SchoolDetail>> response) {
                //Get the SchoolDetail list
                schoolDetailsList.setValue(response.body());
            }
            //If the request is not successful
            @Override
            public void onFailure(Call<ArrayList<SchoolDetail>> call, Throwable t) {
                schoolDetailsList.setValue(null);
            }
        });
    }
}
