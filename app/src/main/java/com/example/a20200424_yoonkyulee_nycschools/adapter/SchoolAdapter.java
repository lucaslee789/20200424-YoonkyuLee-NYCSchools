package com.example.a20200424_yoonkyulee_nycschools.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a20200424_yoonkyulee_nycschools.R;
import com.example.a20200424_yoonkyulee_nycschools.model.SchoolLlist;
import com.example.a20200424_yoonkyulee_nycschools.view.SchoolDetailActivity;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapter.SchoolViewHolder> {

    //Store all the school info and then get the list with constructor
    private Context context;
    private ArrayList<SchoolLlist> NYSchool;

    public SchoolAdapter(Context context, ArrayList<SchoolLlist> NYSchool) {
        this.context = context;
        this.NYSchool = NYSchool;
    }

    @NonNull
    @Override
    public SchoolAdapter.SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        //Inflate View inside of ViewHolder
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.recycler_view_inner_layout, viewGroup,false);
        return new SchoolViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull SchoolAdapter.SchoolViewHolder schoolViewHolder,  final int i) {
        //Get the index of selected school
        final SchoolLlist School = NYSchool.get(i);

        //Bind the data with the ViewHolder
        schoolViewHolder.SchoolName.setText(School.getSchoolName());
        schoolViewHolder.SchoolPhoneNum.setText("Phone: " + School.getPhoneNumber());
        schoolViewHolder.SchoolBorough.setText("Borough: " + School.getBorough());

        //Create OnClickListener to send DBS, NAME, LOCATION, WEBSITE, and OVERVIEW to SchoolDetailActivity
        schoolViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SchoolLlist selection = NYSchool.get(i);
                Intent intent = new Intent(context, SchoolDetailActivity.class);
                intent.putExtra("DBN", selection.getDbn());
                intent.putExtra("NAME",selection.getSchoolName());
                intent.putExtra("LOCATION", selection.getLocation());
                intent.putExtra("WEBSITE",selection.getWebsite());
                intent.putExtra("OVERVIEW",selection.getOverview());
                context.startActivity(intent);
            }
        });
    }

    //Return the size of the collection
    @Override
    public int getItemCount() {
        return NYSchool.size();
    }

    class SchoolViewHolder extends RecyclerView.ViewHolder {
        TextView SchoolName,SchoolPhoneNum,SchoolBorough;
        //Constructor to find the Name, PhoneNum, and Borough
        public SchoolViewHolder(View itemView) {
            super(itemView);
            SchoolName = itemView.findViewById(R.id.SchoolName);
            SchoolPhoneNum = itemView.findViewById(R.id.PhoneNumber);
            SchoolBorough = itemView.findViewById(R.id.Borough);
        }
    }
}
