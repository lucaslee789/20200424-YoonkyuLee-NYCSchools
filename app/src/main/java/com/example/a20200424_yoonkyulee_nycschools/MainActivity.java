package com.example.a20200424_yoonkyulee_nycschools;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.a20200424_yoonkyulee_nycschools.view.SchoolListActivity;

public class MainActivity extends AppCompatActivity {

    private EditText entercode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle(R.string.main_action);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Enter code to direct to the next page. Built Instrumented Test.
        entercode = (EditText)findViewById(R.id.edit_text);
        //Start button
        Button button1 = (Button) findViewById(R.id.enterButton);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = entercode.getText().toString();
                if (text.equals("jpmc")) {
                    Intent intent = new Intent(view.getContext(), SchoolListActivity.class);
                    view.getContext().startActivity(intent);
                }
            }
        });
    }
}
