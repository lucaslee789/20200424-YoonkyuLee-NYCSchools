package com.example.a20200424_yoonkyulee_nycschools;

import android.app.Activity;

import com.example.a20200424_yoonkyulee_nycschools.view.SchoolListActivity;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collection;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry;
import androidx.test.runner.lifecycle.Stage;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
        new ActivityTestRule<>(MainActivity.class);
    //It will pass if you enter "jpmc" as the code, and will fail otherwise.
    @Test
    public void enterCorrectCode() {
        Espresso.onView(ViewMatchers.withId(R.id.edit_text)).perform(ViewActions.typeText("jpmc"));
        Espresso.onView(ViewMatchers.withId(R.id.enterButton)).perform(ViewActions.click());
        Assert.assertEquals(getActivityInstance().getClass(), SchoolListActivity.class);
    }

    private Activity getActivityInstance() {
        final Activity[] currentActivity = {null};
        InstrumentationRegistry.getInstrumentation().runOnMainSync(new Runnable() {
            public void run() {
                Collection resumedActivities = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED);
                if (resumedActivities.iterator().hasNext()) {
                    currentActivity[0] = (Activity) resumedActivities.iterator().next();
                }
            }
        });

        return currentActivity[0];
    }
}
